function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
  /***/
  "./src/app/common/commonComponent.ts":
  /*!*******************************************!*\
    !*** ./src/app/common/commonComponent.ts ***!
    \*******************************************/

  /*! exports provided: BaseComponent */

  /***/
  function srcAppCommonCommonComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BaseComponent", function () {
      return BaseComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/fesm2015/ngx-spinner.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default =
    /*#__PURE__*/
    __webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);

    var BaseComponent =
    /*#__PURE__*/
    function () {
      function BaseComponent(injector) {
        var _this = this;

        _classCallCheck(this, BaseComponent);

        this.router = injector.get(_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]);
        this.platformId = injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["PLATFORM_ID"]);
        this.appId = injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_0__["APP_ID"]);
        this.titleService = injector.get(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"]);
        this.metaService = injector.get(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"]);
        this.activatedRoute = injector.get(_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]);
        this.spinner = injector.get(ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]);
        this.formBuilder = injector.get(_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]);
        this.backToLocation = injector.get(_angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]);
        this.router.events.subscribe(function (event) {
          if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
            _this.routeUrl = event.urlAfterRedirects;
          }
        });
      } // *************************************************************//
      // @Purpose : To check server or browser
      // *************************************************************//


      _createClass(BaseComponent, [{
        key: "isBrowser",
        value: function isBrowser() {
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            return true;
          } else {
            return false;
          }
        } // *************************************************************//
        // @Purpose : We can use following function to use localstorage
        // *************************************************************//

      }, {
        key: "setToken",
        value: function setToken(key, value) {
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            sessionStorage.setItem(key, value);
          }
        }
      }, {
        key: "getToken",
        value: function getToken(key) {
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            return sessionStorage.getItem(key);
          }
        }
      }, {
        key: "removeToken",
        value: function removeToken(key) {
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            sessionStorage.removeItem(key);
          }
        }
      }, {
        key: "clearToken",
        value: function clearToken() {
          if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId)) {
            sessionStorage.clear();
          }
        } // *************************************************************//
        // *************************************************************//
        // @Purpose : We can use following function to use Toaster Service.
        // *************************************************************//

      }, {
        key: "popToast",
        value: function popToast(type, title) {
          sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
            position: 'center',
            // type: type,
            text: title,
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, {
        key: "back",
        value: function back() {
          this.backToLocation.back();
        }
      }, {
        key: "initDataTables",
        value: function initDataTables(domElId) {
          // tslint:disable-next-line:only-arrow-functions
          $(document).ready(function () {
            $('#' + domElId).DataTable({
              'scrollX': true,
              'destroy': true,
              'retrieve': true,
              'stateSave': true,
              'language': {
                'paginate': {
                  'previous': '<i class=\'mdi mdi-chevron-left\'>',
                  'next': '<i class=\'mdi mdi-chevron-right\'>'
                }
              },
              'drawCallback': function drawCallback() {
                $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
              }
            }); // Complex headers with column visibility Datatable
          });
        }
        /****************************************************************************
         @PURPOSE      : To restrict or allow some values in input.
         @PARAMETERS   : $event
         @RETURN       : Boolen
         ****************************************************************************/

      }, {
        key: "RestrictSpace",
        value: function RestrictSpace(e) {
          if (e.keyCode == 32) {
            return false;
          } else {
            return true;
          }
        }
      }, {
        key: "AllowNumbers",
        value: function AllowNumbers(e) {
          var input;

          if (e.metaKey || e.ctrlKey) {
            return true;
          }

          if (e.which === 32) {
            return false;
          }

          if (e.which === 0) {
            return true;
          }

          if (e.which < 33) {
            return true;
          }

          if (e.which === 43 || e.which === 45) {
            return true;
          }

          if (e.which === 36 || e.which === 35) {
            return true;
          }

          if (e.which === 37 || e.which === 39) {
            return true;
          }

          input = String.fromCharCode(e.which);
          return !!/[\d\s]/.test(input);
        }
      }, {
        key: "AllowChar",
        value: function AllowChar(e) {
          if (e.keyCode > 64 && e.keyCode < 91 || e.keyCode > 96 && e.keyCode < 123 || e.keyCode == 8) {
            return true;
          } else {
            return false;
          }
        }
        /****************************************************************************/

        /****************************************************************************/

        /****************************************************************************/

      }, {
        key: "getProfile",
        value: function getProfile() {
          var url = this.getToken('ss_pic');

          if (url == null || url === ' ') {
            return 'assets/images/NoProfile.png';
          } else {
            return url;
          }
        }
        /****************************************************************************
         //For COOKIE
         /****************************************************************************/

      }, {
        key: "setCookie",
        value: function setCookie(name, value, days) {
          var expires = '';

          if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
            expires = '; expires=' + date.toUTCString();
          }

          document.cookie = name + '=' + (value || '') + expires + '; path=/';
        }
      }, {
        key: "getCookie",
        value: function getCookie(name) {
          var nameEQ = name + '=';
          var ca = document.cookie.split(';');

          for (var i = 0; i < ca.length; i++) {
            var c = ca[i];

            while (c.charAt(0) == ' ') {
              c = c.substring(1, c.length);
            }

            if (c.indexOf(nameEQ) == 0) {
              return c.substring(nameEQ.length, c.length);
            }
          }

          return null;
        }
      }, {
        key: "eraseCookie",
        value: function eraseCookie(name) {
          document.cookie = name + '=; Max-Age=-99999999;';
        }
      }, {
        key: "screen_size",
        value: function screen_size() {
          $('.screen-detail ').height($(window).height() - ($('header ').outerHeight() + $('.screen-tab ').outerHeight() + $('footer ').outerHeight()));
        }
      }]);

      return BaseComponent;
    }();
    /***/

  }
}]);
//# sourceMappingURL=common-es5.js.map