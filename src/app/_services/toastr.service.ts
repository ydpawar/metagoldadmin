import {Injectable} from '@angular/core';
declare var toastr: any;

@Injectable()
export class ToastrService {

  constructor() {
    toastr.options = {
      'closeButton': true,
      'debug': false,
      'newestOnTop': false,
      'progressBar': false,
      'positionClass': 'toast-top-right',
      'preventDuplicates': false,
      'onclick': null,
      'showDuration': '300',
      'hideDuration': '1000',
      'timeOut': '2000',
      'extendedTimeOut': '1000',
      'showEasing': 'swing',
      'hideEasing': 'linear',
      'showMethod': 'fadeIn',
      'hideMethod': 'fadeOut',
      'enableHtml': true
    };
    toastr.options.escapeHtml = true;
  }

  success(title: string, messages?: string) {
    toastr.success(title, messages);
  }

  error(title: string, messages?: string) {
    toastr.error(title, messages);
  }

}
