import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SettingService } from '../../../_api/index';
import {Location} from '@angular/common';

@Component({
  selector: 'app-create',
  templateUrl: './index.component.html',
  providers: [SettingService]
})
export class CreateComponent implements OnInit, AfterViewInit {
  frm: FormGroup;

  title = 'Setting - Create';
  breadcrumb: any = [{title: 'Setting', url: '/' }, {title: 'Create', url: '/' }];

  constructor(
    private formBuilder: FormBuilder,
    private service: SettingService,
    private router: Router,
    private route: ActivatedRoute,
    // tslint:disable-next-line:variable-name
    private _location: Location
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      key: ['', Validators.required],
      value: ['', Validators.required],
    });
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.frm.reset();
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
    }
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  onCancel() {
    this._location.back();
  }

  ngAfterViewInit() {
  }

  get frmKey() { return this.frm.get('key'); }
  get frmValue() { return this.frm.get('value'); }

}
