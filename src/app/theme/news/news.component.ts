import { Component, OnInit } from '@angular/core';
import { AuthIdentityService } from 'src/app/_services/auth-identity.service';
import { ToastrService } from 'src/app/_services/toastr.service';
import { NewsService } from 'src/app/_api/news/news.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';

declare var $ , swal;

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit {

  dataList: any = [];
  eventList: any = [];
  newsList: any = [];

  isEmpty = true;
  cUserData: any;

  isLoad: boolean = false;

  constructor(private service: NewsService,
    private authIdentity: AuthIdentityService,
    private spinner: NgxSpinnerService,
    private toaster: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    ) {
      if (authIdentity.isLoggedIn()) {
        this.cUserData = authIdentity.getIdentity();
      }
     }

  ngOnInit() {
    this.getNewsList();
  }


  getNewsList() {
     this.spinner.show();
     this.service.getNewsList().subscribe(
        (data) => {
          // console.log(data);
          this.onSuccessDataList(data);
        },
        error => {
          this.toaster.error(error.message, 'Something want wrong..!');
        });
  }

  onSuccessDataList(response) {
    this.spinner.hide();
      
    if (response.status === 1) {
        this.isEmpty = true;
        this.dataList = response.data ;
        // console.log(this.dataList)
    }
  }

  doDeleteNews(uid){
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.deleteNews(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    this.toaster.success(res.message);

    if (res.status === 1) {
      this.getNewsList();
    }

  }

}
