import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewsService } from 'src/app/_api/news/news.service';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  providers: [NewsService]
})
export class CreateComponent implements OnInit {

  frm: FormGroup;

  title = 'News - Create';
  
  breadcrumb: any = [{title: 'News', url: '/' }, {title: 'Create', url: '/' }];

  constructor(
    private formBuilder: FormBuilder,
    private service: NewsService,
    private router: Router,
    private route: ActivatedRoute,
  ) { 
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      title: ['', Validators.required],
      information: ['', Validators.required],
    });
  }


  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.frm.reset();
      this.service.createNews(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/news']);
    }
  }

  getFormData() {
    const data = this.frm.value
    return data;
  }

  get frmTitle() { return this.frm.get('title'); }
  get frmInformation() { return this.frm.get('information'); }

}
