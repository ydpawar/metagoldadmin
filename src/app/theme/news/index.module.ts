import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsComponent } from './news.component';
import { CreateComponent } from './create/create.component';


const routes: Routes = [
  {
    path: '',
    component: NewsComponent,
  },
  {
    path: 'create',
    component: CreateComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
  ], exports: [
    RouterModule
  ], declarations: [NewsComponent, CreateComponent]
})
export class NewsModule {

}
