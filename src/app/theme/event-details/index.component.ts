import {Component, OnInit, OnDestroy} from '@angular/core';
import {DashboardService} from '../../_api';
import {ActivatedRoute} from '@angular/router';
import {AuthIdentityService, ToastrService} from '../../_services';
import swal from 'sweetalert2';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [DashboardService]
})

export class EventDetailsComponent implements OnInit , OnDestroy {

    title = 'Event Details';
    breadcrumb: any = [{title: 'Event Details', url: '/'}];

    uid: string;
    eid: string;
    sport: string;
    marketName: string;
    isEmpty = true;
    eventData: any = [];
    otherMarket: any = [];
    matchOdd: any = null;
    completedMatch: any = null;
    tiedMatch: any = null;
    bookMaker: any = null;
    virtualCricket: any = null;
    fancyMarket: any = null;
    fancy2Market: any = null;
    fancy3Market: any = null;
    khadoSession: any = null;
    ballSession: any = null;
    jackpotData: any = null;
    cricketCasino: any = null;
    marketIdsArr: any = [];
    marketOdds: any = [];
    betList1: any = [];
    betList2: any = [];
    betList3: any = [];
    betList4: any = [];
    betList5: any = [];
    marketIds: any = [];

    bookData: any  = null;
    bookDataFancy: any = null;
    bookDataCasino: any = null;
    cUserData: any;
    private isDestroy: boolean = false;
    private isFirstLoad: boolean = false;
    tvStatus: boolean = false;

    // tslint:disable-next-line:max-line-length
    constructor(
        private service: DashboardService,
        private route: ActivatedRoute,
        private authIdentity: AuthIdentityService,
        private spinner: NgxSpinnerService,
        private toaster: ToastrService) {

        this.eid = this.route.snapshot.params.eid;
        this.sport = this.route.snapshot.params.sport;

        this.uid = this.route.snapshot.params.uid;

    }

    ngOnInit() {
        this.spinner.show();
        this.getDetail(this.uid, this.eid, this.sport);
        this.getBetList(this.uid, this.eid);
    }

    ngOnDestroy(): void {
        this.isDestroy = true;
    }

    async getDetail(uid, eid, sport) {
        await this.service.getDetail(uid, eid, sport).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessDataList(data);
            },
            error => {
                this.toaster.error(error.message, 'Something want wrong..!');
            });
    }

    onSuccessDataList(response) {
        if (response.status !== undefined) {
            if (response.status === 1) {
                this.isEmpty = true;
                if (response.data.items !== undefined) {
                    this.eventData = response.data.items;
                    this.cUserData = response.data.userData;

                    // MatchOdd
                    if (response.data.items.otherMarket && response.data.items.otherMarket.MatchOdd) {
                        this.matchOdd = response.data.items.otherMarket.MatchOdd;
                    } else { this.matchOdd = null; }
                    // CompletedMatch
                    if (response.data.items.otherMarket && response.data.items.otherMarket.CompletedMatch) {
                        this.completedMatch = response.data.items.otherMarket.CompletedMatch;
                    } else { this.completedMatch = null; }
                    // TiedMatch
                    if (response.data.items.otherMarket && response.data.items.otherMarket.TiedMatch) {
                        this.tiedMatch = response.data.items.otherMarket.TiedMatch;
                    } else { this.tiedMatch = null; }
                    // bookMaker
                    if (response.data.items.bookMaker && response.data.items.bookMaker.length > 0) {
                        this.bookMaker = response.data.items.bookMaker;
                    } else { this.bookMaker = null; }
                    // virtualCricket
                    if (response.data.items.virtualCricket && response.data.items.virtualCricket.length > 0) {
                        this.virtualCricket = response.data.items.virtualCricket;
                    } else { this.virtualCricket = null; }
                    // FancyMarket
                    if (response.data.items.fancyMarket && response.data.items.fancyMarket.length > 0) {
                        this.fancyMarket = response.data.items.fancyMarket;
                    } else { this.fancyMarket = null; }
                    // Fancy2Market
                    if (response.data.items.fancy2Market && response.data.items.fancy2Market.length > 0) {
                        this.fancy2Market = response.data.items.fancy2Market;
                    } else { this.fancy2Market = null; }
                    // Fancy3Market
                    if (response.data.items.fancy3Market && response.data.items.fancy3Market.length > 0) {
                        this.fancy3Market = response.data.items.fancy3Market;
                    } else { this.fancy3Market = null; }
                    // KhadoSession
                    if (response.data.items.khadoSession && response.data.items.khadoSession.length > 0) {
                        this.khadoSession = response.data.items.khadoSession;
                    } else { this.khadoSession = null; }
                    // BallSession
                    if (response.data.items.ballSession && response.data.items.ballSession.length > 0) {
                        this.ballSession = response.data.items.ballSession;
                    } else { this.ballSession = null; }
                    // JackpotData
                    if (response.data.items.jackpotData && response.data.items.jackpotData.length > 0) {
                        this.jackpotData = response.data.items.jackpotData;
                    } else { this.jackpotData = null; }
                    // cricketCasino
                    if (response.data.items.cricketCasino && response.data.items.cricketCasino.length > 0) {
                        this.cricketCasino = response.data.items.cricketCasino;
                    } else { this.cricketCasino = null; }

                    this.marketIdsArr = response.data.marketIdsArr;
                    if (this.marketIdsArr && this.marketIdsArr.length > 0) {
                        // this.isFirstLoad = true;
                        this.getDataOdds(this.marketIdsArr);
                    }
                }

                // tslint:disable-next-line:triple-equals
                if (!this.isDestroy && this.sport != 'live-games') {
                   const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDetail(this.uid, this.eid, this.sport); }, 5000);
                }
            }
        }

        if (!this.isFirstLoad) {
            this.isFirstLoad = true;
            this.spinner.hide();
        }
    }

    async getBetList(uid, eid) {
        await this.service.getBetList(uid, eid).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessBetList(data);
            },
            error => {
                this.toaster.error(error.message, 'Something want wrong..!');
            });
    }

    onSuccessBetList(response) {
        if ( response.status === 1) {
            if (response.data !== undefined) {
                this.betList1 = response.data.betList1;
                this.betList2 = response.data.betList2;
                this.betList3 = response.data.betList3;
                this.betList4 = response.data.betList4;
                this.betList5 = response.data.betList5;
            }

            if (response.marketIds !== undefined) {
                this.marketIds = response.marketIds;
            }

            if (!this.isDestroy) {
                const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getBetList(this.uid, this.eid); }, 3000);
            }
        }
    }

    checkMarket(mid) {
        const ind = this.marketIds.findIndex(x => x === mid);
        if ( ind > 0 ) {
            return true;
        } else {
            return false;
        }
    }

    async getDataOdds(data) {
        await this.service.getOdds(data).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessDataOdds(data);
            },
            error => {
                if (!this.isDestroy) {
                    const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataOdds(this.marketIdsArr); }, 3000);
                }
                // this.toaster.error(error.message, 'Something want wrong..!');
            });
    }

    onSuccessDataOdds(response) {
        if (response.status !== undefined && response.status === 1) {
            if (response.data !== undefined && response.data.items) {
                this.marketOdds = response.data.items;
            }
            if (!this.isDestroy) {
                const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataOdds(this.marketIdsArr); }, 1000);
            }
        } else {
            if (!this.isDestroy) {
                const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getDataOdds(this.marketIdsArr); }, 3000);
            }
        }
    }

    doBetAllowed(eid) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doBetAllowed(eid).subscribe((res) => this.onSuccessBetAllowed(res));
            }
        });
    }

    doBetAllowedMarket(mid, type) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doBetAllowedMarket(mid, type).subscribe((res) => this.onSuccessBetAllowed(res));
            }
        });
    }

    onSuccessBetAllowed(res) {
        if (res.status === 1) {
            this.getDetail(this.uid, this.eid, this.sport);
        }
    }

    doBetDelete(betId) {
        swal.fire({
            title: 'Are you sure to want delete this bet ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service.doBetDelete(betId).subscribe((res) => this.onSuccessBetDelete(res));
            }
        });
    }

    onSuccessBetDelete(res) {
        if (res.status === 1) {
            this.getBetList(this.uid, this.eid);
        }
    }

    getBookDataMatchOdd(uid, mid, marketName) {
        // this.bookData = [];
        this.marketName = marketName;
        const data = {userId: uid, marketId: mid};
        this.service.getBookDataMatchOdd(data).subscribe((res) => this.onSuccessBookDataMatchOdd(res));
    }

    onSuccessBookDataMatchOdd(res) {
        if (res.status === 1) {
            this.bookData = res.data;
        }
    }

    getBookDataBookMaker(uid, mid, marketName) {
        // this.bookData = [];
        this.marketName = marketName;
        const data = {userId: uid, marketId: mid};
        this.service.getBookDataBookMaker(data).subscribe((res) => this.onSuccessBookDataBookMaker(res));
    }

    onSuccessBookDataBookMaker(res) {
        if (res.status === 1) {
            this.bookData = res.data;
        }
    }

    getBookDataFancy(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getBookDataFancy(data).subscribe((res) => this.onSuccessBookDataFancy(res));
        }
    }

    onSuccessBookDataFancy(res) {
        if (res.status === 1) {
            this.bookDataFancy = res.data;
        }
    }

    getBookDataFancy3(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getBookDataFancy3(data).subscribe((res) => this.onSuccessBookDataFancy3(res));
        }
    }

    onSuccessBookDataFancy3(res) {
        if (res.status === 1) {
            this.bookDataFancy = res.data;
        }
    }

    getBookDataBallSession(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getBookDataBallSession(data).subscribe((res) => this.onSuccessBookDataBallSession(res));
        }
    }

    onSuccessBookDataBallSession(res) {
        if (res.status === 1) {
            this.bookDataFancy = res.data;
        }
    }

    getBookDataKhado(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getBookDataKhado(data).subscribe((res) => this.onSuccessBookDataKhado(res));
        }
    }

    onSuccessBookDataKhado(res) {
        if (res.status === 1) {
            this.bookDataFancy = res.data;
        }
    }

    getBookDataCasino(uid, mid, marketName) {
        // this.bookDataFancy = [];
        if ( this.checkMarket(mid) ) {
            this.marketName = marketName;
            const data = {userId: uid, marketId: mid};
            this.service.getBookDataCasino(data).subscribe((res) => this.onSuccessBookDataCasino(res));
        }
    }

    onSuccessBookDataCasino(res) {
        if (res.status === 1) {
            this.bookDataCasino = res.data;
        }
    }


}
