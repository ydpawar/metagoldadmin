import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { ReportsComponent } from './index.component';
import {ExcelService} from '../../common/excel.service';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
    children: [
      {
        path: 'account-statement',
        loadChildren: './account-statement/index.module#AccountStatementModule'
      },
      {
        path: 'profit-loss',
        loadChildren: './profit-loss/index.module#ProfitLossModule'
      },
      {
        path: 'teenpatti-profit-loss',
        loadChildren: './teenpatti-profit-loss/index.module#TeenpattiProfitLossModule'
      },
      {
        path: 'chip-history',
        loadChildren: './chip-history/index.module#ChipHistoryModule'
      },
      {
        path: 'market-bet',
        loadChildren: './market-bet/index.module#MarketBetModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [ReportsComponent],
  providers: [ExcelService]
})
export class ReportsModule {
}
