import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { RulesService } from '../../../_api/index';
import {Location} from '@angular/common';
import {ToastrService} from '../../../_services';

@Component({
  selector: 'app-create',
  templateUrl: './index.component.html',
  providers: [RulesService]
})
export class CreateComponent implements OnInit, AfterViewInit {
  frm: FormGroup;

  title = 'Rules - Create';
  breadcrumb: any = [{title: 'Rules', url: '/' }, {title: 'Create', url: '/' }];
  sportList: any;
  marketList: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: RulesService,
    private router: Router,
    private route: ActivatedRoute,
    private toaster: ToastrService,
    // tslint:disable-next-line:variable-name
    private _location: Location
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getRequiredData();
  }

  async getRequiredData() {
    await this.service.getRequiredData().subscribe(
        // tslint:disable-next-line:no-shadowed-variable
        (data) => {
          this.onSuccessRequiredData(data);
        },
        error => {
          this.toaster.error(error.message, 'Something want wrong..!');
        });
  }

  onSuccessRequiredData(response) {
    if (response.status !== undefined) {
      if (response.status === 1) {
        this.sportList = response.data.sport;
        this.marketList = response.data.market;
      }
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      sport: ['', Validators.required],
      market: ['', Validators.required],
      rules: ['', Validators.required],
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.frm.reset();
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.router.navigate(['/manage']);
    }
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  onCancel() {
    this._location.back();
  }

  ngAfterViewInit() {
  }

  get frmSport() { return this.frm.get('sport'); }
  get frmMarket() { return this.frm.get('market'); }
  get frmRules() { return this.frm.get('rules'); }

}
