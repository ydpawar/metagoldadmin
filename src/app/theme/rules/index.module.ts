import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { RulesComponent } from './index.component';

import { ManageComponent } from './manage/index.component';
import { ManageModule } from './manage/index.module';


import { CreateComponent } from './create/index.component';
import { CreateModule } from './create/index.module';

const routes: Routes = [
  {
    path: '',
    component: RulesComponent,
    children: [
      {
        path: '',
        component: ManageComponent,
      },
      {
        path: 'manage',
        component: ManageComponent,
      },
      {
        path: 'create',
        component: CreateComponent,
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
    ManageModule,
    CreateModule
  ],
  exports: [RouterModule],
  declarations: [RulesComponent],
})
export class RulesModule {
}
