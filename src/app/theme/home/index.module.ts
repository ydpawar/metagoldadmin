import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { MarketOrderComponent } from './market-orders/index.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'market-orders',
    component: MarketOrderComponent,
  },
  {
    path: 'market-orders/:id',
    component: MarketOrderComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    HomeComponent,
    MarketOrderComponent
  ]
})
export class HomeModule {
}
