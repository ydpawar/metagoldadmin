import {AfterViewInit, Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {HistoryService, DashboardService} from '../../../_api';
import {Validators} from '@angular/forms';
import {BaseComponent} from '../../../common/commonComponent';
import {ExcelService} from '../../../common/excel.service';
import {AuthIdentityService, ToastrService} from '../../../_services/index';

declare var $, swal;

// tslint:disable-next-line:class-name
interface listData {
    id: string;
    clientId:string;
    description: string;
    client: string;
    buyPrice: string;
    sellPrice: string;
    closeBuyPrice: string;
    closeSellPrice: string;
    orderType: string;
    sl: string;
    tp: string;
    ip: string;
    master: string;
    time: string;
    status: string;
    role:string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
    id: string;
    clientId:string;
    description: string;
    client: string;
    buyPrice: string;
    sellPrice: string;
    closeBuyPrice: string;
    closeSellPrice: string;
    orderType: string;
    sl: string;
    tp: string;
    ip: string;
    master: string;
    time: string;
    status: string;
    role:string;
}

@Component({
    selector: 'app-home',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [DashboardService]
})

export class MarketOrderComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    title = 'Market Order History';
    breadcrumb: any = [{title: 'Market Order History', url: '/'}];
    page = {start: 1, end: 5};
    uid = '';
    aList: listData[] = [];
    cItem: listData;
    isEmpty = false;
    isLoading = false;
    dataTableId = 'DataTables_current_bet';
    user: any;

    constructor(
        inj: Injector,
        private service: DashboardService,
        private excelSheet: ExcelService,
        private toaster: ToastrService
    ) {
        super(inj);
     
        this.uid = this.activatedRoute.snapshot.params.id;
        // tslint:disable-next-line:triple-equals
        if (window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined) {
            this.title = 'Market Order History for ' + window.localStorage.getItem('title');
        }
        const auth = new AuthIdentityService();

        if (auth.isLoggedIn()) {
          this.user = auth.getIdentity();
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.createForm();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        window.localStorage.removeItem('title');
        window.localStorage.removeItem(this.dataTableId);
    }

    async applyFilters() {
        await this.service.bets(this.uid, {}).subscribe(
            (res) => this.onSuccess(res));
    }

    onSuccess(res) {
        this.spinner.hide();
       // debugger;
        if (res.status !== undefined && res.status === 1) {
            $('#DataTables_current_bet').dataTable().fnDestroy();
            if (res.userName !== undefined) {
                this.title = 'Market Order History for ' + res.userName;
            }
            if (res.data !== undefined) {
                const items = res.data;
                const data: listData[] = [];

                if (items.length > 0) {
                    this.aList = [];
                    for (const item of items) {
                        const cData: listData = new listDataObj();

                        cData.id = item.id;
                        cData.clientId = item.uid;
                        cData.description = item.description;
                        cData.client = item.client;
                        cData.buyPrice = item.buy_price;
                        cData.sellPrice = item.sell_price;
                        // if (item.mType === 'match_odd' || item.mType === 'bookmaker' || item.mType === 'fancy3') {
                        //     cData.rate = '-';
                        // } else {
                        //     cData.rate = item.rate;
                        // }
                        // cData.stake = item.size;
                        // cData.result = item.result;
                        // if (item.result === 'WIN') {
                        //     cData.pl = item.win;
                        // } else if (item.result === 'LOSS') {
                        //     cData.pl = item.loss;
                        // } else {
                        //     if (item.bType === 'back' || item.bType === 'yes') {
                        //         cData.pl = item.win;
                        //     } else {
                        //         cData.pl = item.loss;
                        //     }
                        // }
                        cData.closeBuyPrice = item.closeBuyPrice;
                        cData.closeSellPrice = item.closeSellPrice;
                        cData.orderType = item.order_type;
                        cData.sl = item.stopLoss;
                        cData.tp = item.takeProfit;
                        cData.ip = item.ip_address;
                        cData.master = item.master;
                        cData.time = item.updated_at;
                        cData.status = item.status;
                        cData.role = item.role;

                        data.push(cData);
                    }
                } else {
                    this.isEmpty = true;
                }

                this.aList = data;
                this.page.end = this.page.end + items.length - 1;
                // this.loadJs.load('script' , 'assets/js/datatables.init.js');
                this.loadScript();

            }
        }
    }

    createForm() {
        this.frm = this.formBuilder.group({
            type: ['3', Validators.required],
            start: ['', Validators.required],
            end: ['', Validators.required],
        });
    }

    submitForm(fType = null) {
        if (fType != null) {
            this.spinner.show();
            const data = this.frm.value;
            data.ftype = fType;
            this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
        } else {
            if (this.frm.valid) {
                this.spinner.show();
                const data = this.frm.value;
                this.service.bets(this.uid, data).subscribe((res) => this.onSearch(res));
            }
        }
    }

    onSearch(res) {
        if (res.status === 1) {
           // this.frm.reset();
            this.onSuccess(res);
        }
    }

    loadScript() {
        // tslint:disable-next-line:only-arrow-functions
        $(document).ready(function() {
            $('#DataTables_current_bet').DataTable({
                'scrollX': true,
                'destroy': true,
                'retrieve': true,
                'stateSave': true,
                'language': {
                    'paginate': {
                        'previous': '<i class=\'mdi mdi-chevron-left\'>',
                        'next': '<i class=\'mdi mdi-chevron-right\'>'
                    }
                },
                'drawCallback': function drawCallback() {
                    $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
                }
            }); // Complex headers with column visibility Datatable
        });

    }

    excelDownload() {
        const tmpData = [];
        let tmp = {
            ORDER_ID: '',
            CLIENT_ID:'',
            DESCRIPTION: '',
            CLIENT: '',
            BUY_PRICE: '',
            SELL_PRICE: '',
            CLOSE_BUY_PRICE: '',
            CLOSE_SELL_PRICE: '',
            ORDER_TYPE: '',
            SL: '',
            TP: '',
            IP: '',
            MASTER: '',
            TIME: '',
        };
        tmpData.push(tmp);
        this.aList.forEach((item, index) => {
            tmp = {
                ORDER_ID: item.id,
                CLIENT_ID: item.clientId,
                DESCRIPTION: item.description,
                CLIENT: item.client,
                BUY_PRICE: item.buyPrice,
                SELL_PRICE: item.sellPrice,
                CLOSE_BUY_PRICE: item.closeBuyPrice,
                CLOSE_SELL_PRICE: item.closeSellPrice,
                ORDER_TYPE: item.orderType,
                SL: item.sl,
                TP: item.tp,
                IP: item.ip,
                MASTER: item.master,
                TIME: item.time,
            };
            tmpData.push(tmp);
        });
        this.excelSheet.exportAsExcelFile(tmpData, 'bet-list');
    }

    get frmType() { return this.frm.get('type'); }
    get frmStart() { return this.frm.get('start'); }
    get frmEnd() { return this.frm.get('end'); }

    doStatusDelete(uid) {
        swal.fire({
          title: 'Are you sure to delete ?',
          // text: 'Are you sure to logout?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.value) {
            this.onSuccessStatusUpdate(uid);
            }
        });
    }

    onSuccessStatusUpdate(uid) {
        this.service.doDelete(uid).subscribe((res: any) => {
            if(res.status === 1) {
                this.toaster.success(res.message);
                this.applyFilters()
            } else {
                this.toaster.error('Somthing want to be wrong..!')
            }
        });
        
    }

}
