import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SettingComponent } from './index.component';

const routes: Routes = [
  {
    path: 'setting/:uid',
    component: SettingComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SettingComponent
  ]
})
export class SettingModule {

}
