import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {ActionService, SupervisorService} from '../../../../_api/index';
import {AuthIdentityService} from '../../../../_services/index';
import {BaseComponent} from '../../../../common/commonComponent';

declare var $ , swal;
interface User {
  id: string;
  name: string;
  username: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
}

class UserObj implements User {
  id: string;
  name: string;
  username: string;
  isBlock: {
    text: string;
    class1: string;
    class2: string;
  };
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SupervisorService, ActionService]
})

export class ManageComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  page = { start: 1, end: 5 };
  user: any;
  aList: User[] = [];
  cItem: User;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_sv';

  title = 'Supervisor';
  createUrl = '/users/supervisor/create';
  breadcrumb: any = [{title: 'Supervisor', url: '/' }, {title: 'Manage', url: '/' }];

  constructor(
      inj: Injector,
      private service: SupervisorService,
      private service2: ActionService,
  ) {
    super(inj);
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
      // tslint:disable-next-line:triple-equals
      if ( this.user.role != 'ADMIN' && this.user.role != 'ADMIN2' && this.user.role != 'SM1') {
        this.back();
      }
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_sv').dataTable().fnDestroy();
        const items = res.data;
        const data: User[] = [];

        if (items.length > 0) {

          this.aList = [];
          for (const item of items) {
            const cData: User = new UserObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.username = item.username;
            cData.isBlock = {
              text: item.isBlock === 1 ? 'Unblock' : 'Block',
              class1: item.isBlock === 1 ? 'success' : 'danger',
              class2: item.isBlock === 1 ? 'mdi-check' : 'mdi-close'
            };

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_sv');
      }
    }
    this.spinner.hide();
  }

  doStatusUpdate(uid, typ) {
    swal.fire({
      title: 'Are you sure to change this status ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if ( typ === 1 ) {
          this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        } else {
          this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
        }
      }
    });
  }

  doStatusDelete(uid) {
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service2.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

}

