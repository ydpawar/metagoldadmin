import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {HistoryService} from '../../_api';
import {ScriptLoaderService} from '../../_services';
import {ActivatedRoute} from '@angular/router';
import swal from "sweetalert2";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';

declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  runner: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  runner: string;
  client: string;
  price: string;
  rate: string;
  stake: string;
  pl: string;
  ip: string;
  master: string;
  time: string;
  bType: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class MarketBetListComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  title = 'Market Bet List';
  breadcrumb: any = [{title: 'Market Bet List', url: '/' }];

  page = { start: 1, end: 5 };

  mtype: string;
  mid: string;
  eid: string;
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  cUserData: any;
  dataTableId = 'DataTables_market_bet';
  private isDestroy: boolean = false;
  private isFirstLoad: boolean = false;

  constructor(
      private formBuilder: FormBuilder,
      private service: HistoryService,
      private loadJs: ScriptLoaderService,
      private route: ActivatedRoute,
      private spinner: NgxSpinnerService
  ) {
    this.mtype = this.route.snapshot.params.mtype;
    this.mid = this.route.snapshot.params.mid;
    this.eid = this.route.snapshot.params.eid;
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
    this.isDestroy = true;
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    const data = { mtype: this.mtype, mid: this.mid, eid: this.eid };
    this.service.marketBets(data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      this.cUserData = res.userData;
      if ( res.userData.name !== undefined ) {
        this.title = 'Market Bet List for ' + res.userData.name;
      }

      if (res.data !== undefined ) {
        const items = res.data.list;
        const data: listData[] = [];

        if ( res.data.count > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.runner = item.runner;
            cData.client = item.client;
            cData.price = item.price;
            if ( item.mType === 'match_odd' || item.mType === 'bookmaker' || item.mType === 'fancy3' ) {
              cData.rate = '-';
            } else {
              cData.rate = item.rate;
            }
            cData.stake = item.size;
            if ( item.bType === 'back' || item.bType === 'yes') {
              cData.pl = item.win;
            } else {
              cData.pl = item.loss;
            }
            cData.ip = item.ip_address;
            cData.master = item.master;
            cData.time = item.created_on;
            cData.bType = item.bType;

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
      }

      if (!this.isDestroy) {
        const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.applyFilters(); }, 5000);
      }
    }
    if ( !this.isFirstLoad ) {
      this.isFirstLoad = true;
      this.spinner.hide();
    }
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_market_bet').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  doBetDelete(betId) {
    swal.fire({
      title: 'Are you sure to want delete this bet ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        this.service.doBetDelete(betId).subscribe((res) => this.onSuccessBetDelete(res));
      }
    });
  }

  onSuccessBetDelete(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      client: [''],
      runner: [''],
    });
  }

  submitForm() {
    if (this.frm.valid) {
      this.spinner.show();
      const data = this.frm.value;
      data.mtype = this.mtype; data.mid = this.mid; data.eid = this.eid;
      this.service.marketBets(data).subscribe((res) => this.onSearch(res));
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      this.isDestroy = true;
      this.onSuccess(res);
    }
  }

  get frmClient() { return this.frm.get('client'); }
  get frmRunner() { return this.frm.get('runner'); }

}
