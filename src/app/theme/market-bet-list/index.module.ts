import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MarketBetListComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: MarketBetListComponent,
  },
  {
    path: ':mtype/:eid',
    component: MarketBetListComponent,
  },
  {
    path: ':mtype/:eid/:mid',
    component: MarketBetListComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    MarketBetListComponent
  ]
})
export class MarketBetListModule {
}
