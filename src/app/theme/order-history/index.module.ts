import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OrderHistoryComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: OrderHistoryComponent,
  },
  {
    path: ':id',
    component: OrderHistoryComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    OrderHistoryComponent
  ]
})
export class OrderHistoryModule {
}
